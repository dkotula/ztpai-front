import React, { Component } from 'react'
import '../css/matrix.css';
import Item from './item'
import axios from 'axios';
import ItemAllTasks from "./itemAllTasks";

export default class Matrix extends Component {
    state = {
        tasks: [],
        tasksEdit: [],
        tasksSelect: [],
        header: ""
    }

    setTasksEdit = (tasks) => {
        this.setState({ tasksEdit: tasks });
    }

    getTasksEdit = () => {
        return this.state.tasksEdit;
    }

    componentDidMount() {
        document.querySelector(".editTask").style.display = "none";
        axios.get("http://localhost:3001/tasks/findAll")
            .then(res => {
                const tasks = res.data;
                this.setState({ tasks: tasks });
                this.setState({ tasksEdit: tasks });
                this.setState({ tasksSelect: tasks });
                document.querySelectorAll(".allTasksToVanish").forEach((el) => el.style.display = "none");
            })
            .catch(() => {
                window.location = "/login";
            });
    }

    getTasks = (name) => {
        let dateNow = new Date().getTime();
        switch (name) {
            case "getUrgAndImp":
                return this.state.tasks.filter(function (task) {
                    let date = new Date(task.deadline).getTime();
                    return task.priority > 5 && (parseInt(date / 1000) - parseInt(dateNow / 1000) - task.making_time) < 2 * 24 * 60 * 60;
                });
            case "getUrgAndNotImp":
                return this.state.tasks.filter(function (task) {
                    let date = new Date(task.deadline).getTime();
                    return task.priority < 6 && (parseInt(date / 1000) - parseInt(dateNow / 1000) - task.making_time) < 2 * 24 * 60 * 60;
                });
            case "getNotUrgAndImp":
                return this.state.tasks.filter(function (task) {
                    let date = new Date(task.deadline).getTime();
                    return task.priority > 5 && (parseInt(date / 1000) - parseInt(dateNow / 1000) - task.making_time) > 2 * 24 * 60 * 60;
                });
            case "getNotUrgAndNotImp":
                return this.state.tasks.filter(function (task) {
                    let date = new Date(task.deadline).getTime();
                    return task.priority < 6 && (parseInt(date / 1000) - parseInt(dateNow / 1000) - task.making_time) > 2 * 24 * 60 * 60;
                });
            default:
                return "Error";
        }
    }

    showItem = (tasks, header) => {
        this.setState({
            "tasksSelect": tasks,
            "tasksEdit": tasks,
            "header": header
        });
        document.querySelectorAll(".itemOfMatrix").forEach((el) => el.style.display = "none");
        document.querySelectorAll(".allTasksToVanish").forEach((el) => el.style.display = "flex");
        document.querySelectorAll(".returnButton").forEach((el) => el.style.top = "0%");
    }

    returnToMatrix = () => {
        document.querySelectorAll(".itemOfMatrix").forEach((el) => el.style.display = "block");
        document.querySelectorAll(".allTasksToVanish").forEach((el) => el.style.display = "none");
        document.querySelectorAll(".editTask").forEach((el) => el.style.display = "none");
        document.querySelectorAll(".returnButton").forEach((el) => el.style.top = "-100%");
    }

    render() {
        return (
            <section className="matrix">
                <Item value={this.getTasks("getUrgAndImp")} header={"Urgent and important"} click={this.showItem} />
                <Item value={this.getTasks("getUrgAndNotImp")} header={"Urgent but not important"} click={this.showItem} />
                <Item value={this.getTasks("getNotUrgAndImp")} header={"Not urgent but important"} click={this.showItem} />
                <Item value={this.getTasks("getNotUrgAndNotImp")} header={"Not urgent and not important"} click={this.showItem} />
                <p className="returnButton" onClick={this.returnToMatrix}>Return</p>
                <ItemAllTasks header={this.state.header} tasks={this.state.tasksSelect} setTasksEdit={this.setTasksEdit} getTasksEdit={this.getTasksEdit} />
            </section>
        )
    }
}