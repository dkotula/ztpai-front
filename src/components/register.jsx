import React, {Component} from 'react'
import '../css/login.css';
import axios from "axios";
import Logo from "./logo";

export default class Register extends Component {
    state = {
        email: '',
        password: '',
        name: '',
        surname: '',
        phone: ''
    }

    constructor(props, context) {
        super(props, context);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let email = this.state.email;
        let password = this.state.password;
        let name = this.state.name;
        let surname = this.state.surname;
        let phone = this.state.phone;
        let state = {
            user: {
                email: email,
                password: password,
            },
            userDetails: {
                name: name,
                surname: surname,
                phone: phone
            }
        }
        axios.post(`http://localhost:3001/users/register`, { state })
            .then(res => {
                if(res.data !== "Success") {
                    alert(res.data);
                }
                else {
                    window.location = "/login";
                }
            })
    }

    render() {
        return (
            <div className="login-container register-container">
                <Logo/>
                <form onSubmit={this.handleSubmit}>
                    <div className="messages">
                    </div>
                    <label>
                        Email:
                        <input type="email" name="email" value={this.state.email} onChange={this.handleChange} placeholder="email@email.com" required />
                    </label>
                    <label>
                        Password:
                        <input type="password" name="password" value={this.state.password} onChange={this.handleChange} placeholder="password" minLength="8" required />
                    </label>
                    <label>
                        Name:
                        <input type="text" name="name" value={this.state.name} onChange={this.handleChange} placeholder="name" required />
                    </label>
                    <label>
                        Surname:
                        <input type="text" name="surname" value={this.state.surname} onChange={this.handleChange} placeholder="surname" required />
                    </label>
                    <label>
                        Phone:
                        <input type="text" name="phone" value={this.state.phone} onChange={this.handleChange} placeholder="123123123" required />
                    </label>
                    <input type="submit" value="Register" />
                    <a href="login">Return to login</a>
                </form>
            </div>
        )
    }
}