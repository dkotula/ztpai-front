import React, { Component } from 'react'
import '../css/button.css';
import { Link } from "react-router-dom";

export default class Button extends Component {
    render() {
        return (
            <li>
                <Link to={this.props.href}>
                        <i className={this.props.icon}/>
                        <p>{this.props.value}</p>
                </Link>
            </li>
        )
    }
}