import React, { Component } from 'react'
import '../css/form.css';

export default class FormTask extends Component {
    state = {
        category_id: '',
        title: '',
        deadline: '',
        making_time: '',
        priority: 5,
        description: '',
        making_time_days: 0,
        making_time_hours: 0,
        making_time_minutes: 0,
    }

    constructor(ref) {
        super(ref);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    async handleSubmit(event) {
        let isOk = true;
        if(!this.state.deadline) {
            isOk = false;
            alert("Deadline cannot be empty!")
        }
        if(!this.state.description) {
            isOk = false;
            alert("Description cannot be empty!")
        }
        if(!this.state.priority) {
            isOk = false;
            alert("priority cannot be empty!")
        }
        if(this.state.making_time_days === 0 && this.state.making_time_hours === 0 && this.state.making_time_minutes === 0) {
            isOk = false;
            alert("making_time cannot be empty!")
        }
        if(!this.state.title) {
            isOk = false;
            alert("title cannot be empty!")
        }
        if(isOk) {
            await this.setState({"making_time": parseInt(this.state.making_time_days) * 1440 + parseInt(this.state.making_time_hours) * 60 + parseInt(this.state.making_time_minutes)});
            this.props.value(this.state);
        }
        event.preventDefault();
    }

    set(task) {
        let date = new Date(task.deadline);
        const dateTimeLocalValue = (new Date(date.getTime() - date.getTimezoneOffset() * 60000).toISOString()).slice(0, -1);
        this.setState({
            "title": task.title,
            "deadline": dateTimeLocalValue,
            "making_time": task.making_time,
            "priority": task.priority,
            "description": task.description,
            "category_id": task.category_id
        });
        let copy_time = task.making_time;

        this.setState({
            "making_time_days": 0,
            "making_time_hours": 0,
            "making_time_minutes": 0
        });

        if (copy_time >= 1440) {
            let days = (copy_time / 1440) | 0;
            this.setState({"making_time_days": days});
            copy_time -= days * 1440;
        }

        if (copy_time >= 60) {
            let hours = (copy_time / 60) | 0;
            this.setState({"making_time_hours": hours});
            copy_time -= hours * 60;
        }

        if (copy_time > 0) {
            this.setState({"making_time_minutes": copy_time});
        }
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} className="formTask">
                <label>
                    Title:
                    <input type="text" name="title" value={this.state.title} onChange={this.handleChange} />
                </label>
                <label>
                    Deadline:
                    <input type="datetime-local" name="deadline" value={this.state.deadline} onChange={this.handleChange} />
                </label>
                <div>
                    Making time:
                    <div className={"making_time"}>
                        <div>
                            <label>
                                Days:
                                <input type="number" name="making_time_days" min="0" value={this.state.making_time_days} onChange={this.handleChange} />
                            </label>
                        </div>
                        <div>
                            <label>
                                Hours:
                                <input type="number" name="making_time_hours" min="0" max="23" value={this.state.making_time_hours} onChange={this.handleChange} />
                            </label>
                        </div>
                        <div>
                            <label>
                                Minutes:
                                <input type="number" name="making_time_minutes" min="0" max="59" value={this.state.making_time_minutes} onChange={this.handleChange} />
                            </label>
                        </div>
                    </div>
                </div>
                <label>
                    Priority:
                    <input type="number" name="priority" value={this.state.priority} min="1" max="10" onChange={this.handleChange} />
                </label>
                <label>
                    Description:
                    <textarea name="description" value={this.state.description} onChange={this.handleChange} />
                </label>
                <input type="submit" value="Add" />
            </form>
        )
    }
}