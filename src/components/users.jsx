import React, {Component} from 'react'
import '../css/users.css';
import axios from "axios";

export default class Users extends Component {
    state = {
        users: []
    }

    componentDidMount() {
        axios.get("http://localhost:3001/users/findAll")
            .then(res => {
                const users = res.data;
                this.setState({users: users});
            })
            .catch(() => {
                window.location = "/login";
            });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        axios.delete(`http://localhost:3001/users/delete` + event.target.parentNode.getAttribute("name"))
            .then(res => {
                window.location = "/users";
            })
            .catch(() => {
                window.location = "/login";
            });
    }

    render() {
        return (
            <section className="users">
                {
                    this.state.users.map(user =>
                        <div className="user" key={"d" + user.id} name={user.id}>
                            <p key={"t1" + user.id}><b>Email:</b> {user.email}</p>
                            <button onClick={this.handleSubmit} key={"b" + user.id} name="Delete">Delete</button>
                        </div>
                    )
                }
            </section>
        )
    }
}