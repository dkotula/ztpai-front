import React, { Component } from 'react'
import makingTime from "../js/makingTime"

export default class Hover extends Component {
    constructor(props) {
        super(props);
        this.handleMouseHover = this.handleMouseHover.bind(this);
        this.state = {
            isHovering: false,
        };
    }

    handleMouseHover() {
        this.setState(this.toggleHoverState);
    }

    toggleHoverState(state) {
        return {
            isHovering: !state.isHovering,
        };
    }

    returnTime(timestamp) {
        let date = new Date(Date.parse(timestamp));
        return date.toLocaleString();
    }

    render() {
        return (
            <div onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                <p><b>Title:</b> {this.props.task.title}</p>
                <p><b>Deadline:</b> {this.returnTime(this.props.task.deadline)}</p>
                {
                    this.state.isHovering &&
                    <div>
                        <p><b>Description:</b> {this.props.task.description}</p>
                        <p><b>Making time:</b> {makingTime(this.props.task.making_time)}</p>
                        <p><b>Priority:</b> {this.props.task.priority}</p>
                    </div>
                }
            </div>
        )
    }
}