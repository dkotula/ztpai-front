import React, { Component } from 'react'
import '../css/editTask.css';
import FormTask from "./formTask";

export default class EditTask extends Component {
    constructor(props) {
        super(props);
        this.child = React.createRef();
    }

    returnState = (state) => {
        let d = new Date(state.deadline);
        state.deadline = Date.parse(d);
        this.props.value(state);
    }

    setCategory = () => {
        let task = this.props.task();
        this.child.current.set(task);
        document.querySelector(".formTask").style.display = "flex";
    }

    render() {
        return (
            <div className="editTask">
                <h2>Edit task</h2>
                <FormTask value={this.returnState} ref={this.child}/>
            </div>
        )
    }
}