import React, { Component } from 'react'
import '../css/add.css';
import FormTask from "./formTask";
import FormCategory from "./formCategory";
import axios from "axios";

export default class Add extends Component {

    categoryId = '';

    returnState = (state) => {
        state.category_id = this.categoryId;
        let d = new Date(state.deadline);
        state.deadline = Date.parse(d);

        axios.post(`http://localhost:3001/tasks/add`, { state })
            .then(res => {
                window.location = "/home";
            })
            .catch(() => {
                window.location = "/login";
            });
    }

    returnId = (id) => {
        this.categoryId = id;
    }

    render() {
        return (
            <section className="add">
                <h2>Add new task</h2>
                <FormCategory value={this.returnId}/>
                <FormTask value={this.returnState}/>
            </section>
        )
    }
}
