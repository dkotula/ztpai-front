import React, {useState} from 'react';
import '../css/itemAllTask.css';
import axios from 'axios';
import EditTask from "./editTask";
import makingTime from "../js/makingTime"
import SearchBar from "./Search";

let task = {};

const ItemAllTasks = (props) => {
    const child = React.createRef();
    const [input, setInput] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        if (event.target.name === "Delete") {
            axios.delete(`http://localhost:3001/tasks/delete` + event.target.parentNode.parentNode.getAttribute("name"))
                .then(res => {
                    window.location = "/views";
                })
                .catch(() => {
                    window.location = "/login";
                });
        } else {
            task = props.tasks.find(element => element.id === parseInt(event.target.parentNode.parentNode.getAttribute("name")));
            document.querySelector(".editTask").style.display = "flex";
            document.querySelectorAll(".allTasksToVanish").forEach((el) => el.style.display = "none");
            child.current.setCategory();
        }
    }

    const getTask = () => {
        return task;
    }

    const returnState = (state) => {
        axios.put(`http://localhost:3001/tasks/change` + task.id, {state})
            .then(res => {
                window.location = "/views";
            })
            .catch(() => {
                window.location = "/login";
            });
    }

    const returnTime = (timestamp) => {
        let date = new Date(Date.parse(timestamp));
        return date.toLocaleString();
    }

    const updateInput = async (input) => {
        const filtered = props.tasks.filter(task => {
            return task.title.toLowerCase().includes(input.toLowerCase()) ||
                task.description.toLowerCase().includes(input.toLowerCase()) ||
                returnTime(task.deadline).includes(input);
        })
        setInput(input);
        props.setTasksEdit(filtered);
    }

    return (
        <div>
            <h2 className={"allTasksToVanish"}>{props.header}</h2>
            <SearchBar input={input} onChange={updateInput}/>
            {
                props.getTasksEdit().map((task) =>
                    <div className="allTasksToVanish task" key={"d" + task.id} name={task.id}>
                        <p key={"t1" + task.id}><b>Title:</b> {task.title}</p>
                        <p key={"t2" + task.id}><b>Making time:</b> {makingTime(task.making_time)}</p>
                        <p key={"t3" + task.id}><b>Priority:</b> {task.priority}</p>
                        <p key={"t4" + task.id}><b>Description:</b> {task.description}</p>
                        <p key={"t5" + task.id}><b>Deadline:</b> {returnTime(task.deadline)}</p>
                        <div>
                            <button onClick={handleSubmit} key={"b1" + task.id} name="Edit">Edit</button>
                            <button onClick={handleSubmit} key={"b2" + task.id} name="Delete">Delete</button>
                        </div>
                    </div>
                )
            }
            <EditTask value={returnState} task={getTask} ref={child}/>
        </div>
    );
}

export default ItemAllTasks