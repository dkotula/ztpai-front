import React, { Component } from 'react'
import '../css/form.css';
import Select from "react-select";
import axios from "axios";

export default class FormCategory extends Component {

    options = []
    state = {
        category_name: ''
    }
    categoryId = ''

    componentDidMount() {
        axios.get("http://localhost:3001/categories/findAll")
            .then(res => {
                const categories = res.data;
                for (let i = 0; i < categories.length; i++) {
                    this.options[i] = {value: categories[i].id, label: categories[i].category_name}
                }
            })
            .catch(() => {
                window.location = "/login";
            });
    }

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            category_name: event.target.value
        });
    }

    handleSubmit(event) {
        if (event.target.name === "2") {
            if(!this.categoryId) {
                alert("Select category!");
            }
            else {
                this.changeView(this.categoryId);
            }
        }
        else {
            if(!this.state.category_name) {
                alert("Cannot add empty category!");
            }
            else {
                const category = {
                    category_name: this.state.category_name
                };

                axios.post(`http://localhost:3001/categories/add`, { category })
                    .then(res => {
                        this.changeView(res.data.id);
                    })
                    .catch(() => {
                        window.location = "/login";
                    });
            }
        }
        event.preventDefault();
    }

    changeView(id) {
        document.querySelector(".formTask").style.display = "flex";
        document.querySelector(".formCategoryDiv").style.display = "none";
        this.props.value(id);
    }

    render() {
        return (
            <div className="formCategoryDiv">
                <form className="formCategory" name="1" onSubmit={this.handleSubmit}>
                    <label>
                        New category:
                        <input type="text" name="category_name" value={this.state.category_name} onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="Add new category" name="input1" />
                </form>
                OR
                <form className="formCategory" name="2" onSubmit={this.handleSubmit}>
                    <label>
                        Select category:
                        <Select
                            options={this.options}
                            onChange={value => this.categoryId = value.value}
                        />
                    </label>
                    <input type="submit" value="Select existing" name="input2" />
                </form>
            </div>
        )
    }
}