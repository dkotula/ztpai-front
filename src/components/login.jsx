import React, {Component} from 'react'
import '../css/login.css';
import axios from "axios";
import Logo from "./logo";
import jwt_decode from "jwt-decode";

export default class Login extends Component {
    state = {
        email: '',
        password: ''
    }

    constructor(props, context) {
        super(props, context);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (event) => {
        let state = this.state;
        axios.post(`http://localhost:3001/auth/login`, { state })
            .then(res => {
                if (res.data.err) {
                    alert(res.data.err);
                }
                else if (res.data) {
                    const decoded = jwt_decode(res.data.access_token);
                    localStorage.setItem('token', res.data.access_token);
                    localStorage.setItem('role', decoded.user.role);
                    window.location = "/";
                }
            })
        event.preventDefault();
    }

    render() {
        return (
            <div className="login-container">
                <Logo/>
                <form onSubmit={this.handleSubmit}>
                    <div className="messages">
                    </div>
                    <label>
                        Email:
                        <input type="text" name="email" value={this.state.email} onChange={this.handleChange} placeholder="email@email.com" />
                    </label>
                    <label>
                        Password:
                        <input type="password" name="password" value={this.state.password} onChange={this.handleChange} placeholder="password" />
                    </label>
                    <input type="submit" value="Login" />
                    <a href="register">or sign up</a>
                </form>
            </div>
        )
    }
}