import React, {Component} from 'react'
import '../css/allTasks.css';
import ItemAllTasks from './itemAllTasks'
import axios from 'axios';

export default class AllTasks extends Component {
    state = {
        tasks: [],
        tasksEdit: []
    }

    setTasksEdit = (tasks) => {
        this.setState({ tasksEdit: tasks });
    }

    getTasksEdit = () => {
        return this.state.tasksEdit;
    }

    componentDidMount() {
        axios.get("http://localhost:3001/tasks/findAll")
            .then(res => {
                const tasks = res.data;
                this.setState({ tasks: tasks });
                this.setState({ tasksEdit: tasks });
            })
            .catch(() => {
                window.location = "/login";
            });
    }
    render() {
        return (
            <section className="allTasks">
                <ItemAllTasks header={"All tasks"} tasks={this.state.tasks} setTasksEdit={this.setTasksEdit} getTasksEdit={this.getTasksEdit} />
            </section>
        )
    }
}