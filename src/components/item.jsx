import React, { Component } from 'react'
import '../css/item.css';
import Hover from "./hover";

export default class Item extends Component {
    render() {
        return (
            <div className={"itemOfMatrix"} onClick={() => this.props.click(this.props.value, this.props.header)}>
                <h2>{this.props.header}</h2>
                {
                    this.props.value.map((task) =>
                        <Hover key={"h" + task.id} task={task}/>
                    )
                }
            </div>
        )
    }
}