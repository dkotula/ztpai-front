import React from 'react';
import '../css/search.css';

const SearchBar = (props) => {
    return (
        <input
            className={"allTasksToVanish searchbar"}
            key="random1"
            value={props.input}
            placeholder="search task &#xF002;"
            onChange={(e) => {
                props.onChange(e.target.value);
            }}
        />
    );
}

export default SearchBar