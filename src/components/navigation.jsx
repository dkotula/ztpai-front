import React, { Component } from 'react'
import '../css/navigation.css';
import Button from './button'
import axios from "axios";

export default class Navigation extends Component {
    handleSubmit() {
        localStorage.removeItem('token');
        localStorage.removeItem('role');
        window.location = "/login";
    }

    getTokenInfo() {
        axios.get(`http://localhost:3001/profile`, { })
            .then((res) => {
            })
            .catch(() => {
                window.location = "/login";
            });
    }

    render() {
        this.getTokenInfo();
        return (
            <nav>
                <ul>
                    <Button value={"Home"} href={"/"} icon={"fas fa-home"}/>
                    <Button value={"Add"} href={"/add"} icon={"fas fa-plus"}/>
                    <Button value={"Views"} href={"/views"} icon={"fas fa-tasks"}/>
                    <Button value={"Groups"} href={"/groups"} icon={"fas fa-users"}/>
                    <Button value={"Settings"} href={"/settings"} icon={"fas fa-cog"}/>
                    {
                        localStorage.getItem('role') === "Admin" &&
                        <Button value={"Users"} href={"/users"} icon={"fas fa-users"}/>
                    }
                </ul>
                <button type="submit" className='logout' onClick={this.handleSubmit}>LOGOUT</button>
            </nav>
        )
    }
}