import React, { Component } from 'react'
import '../css/logo.css';

export default class Logo extends Component {
    render() {
        return (
            <header>
                <img src="./images/logo.png" alt="error" />
            </header>
        )
    }
}