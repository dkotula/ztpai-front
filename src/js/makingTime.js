export default function makingTime(time) {
    let copy_time = time;
    let making_time = "";

    if (copy_time >= 1440) { //At least one day
        let days = (copy_time / 1440) | 0;
        making_time += days + " Day";
        if (days > 1) {
            making_time += "s";
        }
        making_time += " ";
        copy_time -= days * 1440;
    }

    if (copy_time >= 60) { //At least one hour
        let hours = (copy_time / 60) | 0;
        making_time += hours + " Hour";
        if (hours > 1) {
            making_time += "s";
        }
        making_time += " ";
        copy_time -= hours * 60;
    }

    if (copy_time > 0) { //At least one minute
        making_time += copy_time + " Minute";
        if (copy_time > 1) {
            making_time += "s";
        }
        making_time += " ";
    }

    return making_time;
}