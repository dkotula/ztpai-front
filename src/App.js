import './css/App.css';
import Logo from './components/logo'
import Navigation from './components/navigation'
import Matrix from './components/matrix'
import Add from './components/add'
import AllTasks from './components/allTasks'
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Login from "./components/login";
import Register from "./components/register";
import Users from "./components/users";
import axios from "axios";

const apiUrl = 'http://localhost:3001';

axios.interceptors.request.use(
    config => {
        const { origin } = new URL(config.url);
        const allowedOrigins = [apiUrl];
        const token = localStorage.getItem('token');
        if (allowedOrigins.includes(origin)) {
            config.headers.authorization = `Bearer ${token}`;
        }
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

function App() {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route path="/login">
                        <Login/>
                    </Route>
                    <Route path="/register">
                        <Register/>
                    </Route>
                    <Route path="/">
                        <Navigation/>
                        <div className="main">
                            <Logo/>
                            <Switch>
                                <Route path="/add">
                                    <Add/>
                                </Route>
                                <Route path="/views">
                                    <AllTasks/>
                                </Route>
                                <Route path="/groups">
                                    <Matrix/>
                                </Route>
                                <Route path="/settings">
                                    <Matrix/>
                                </Route>
                                <Route path="/users">
                                    <Users/>
                                </Route>
                                <Route path="/">
                                    <Matrix/>
                                </Route>
                            </Switch>
                        </div>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;